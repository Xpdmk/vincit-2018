import React, { Component } from 'react';
import Sighting from './components/Sighting';
import Helmet from 'react-helmet';
import Card, {CardContent} from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import FormDialog from './components/FormDialog';


class App extends Component {
  constructor() {
    super()
    this.state = {
      data: null,
      species: null
    }
  }

  componentDidMount() {
    fetch("http://localhost:8081/sightings")
      .then(text => text.json())
      .then(data => this.setState({data: data}))
    fetch("http://localhost:8081/species")
      .then(text => text.json())
      .then(data => this.setState({species: data}))
  }

  render() {
    return (
      <div style={styles.background}>
        <div style={styles.content}>
          <Helmet>
            <title>Vincit 2018</title>
            <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet" />
            <style>
            {`
              body {
                font-family: Roboto;
              }
            `}
            </style>
          </Helmet>
          <div style={styles.topContainer}>
            <FormDialog open={true} species={this.state.species} label="Report new sighting"/>
          </div>
          <Card>
            <CardContent>
            {this.state.data
              ? this.state.data.map((sighting, index) =>
                <div index={index} key={sighting.id}>
                  <Sighting sighting={sighting} />
                  {index < this.state.data.length - 1
                    ? <Divider style={styles.list.divider}/> 
                    : null}
                </div>)
              : <p>Fetching data...</p>
            }
            </CardContent>
          </Card>
        </div>
      </div>
    );
  }
}

const defaults = {
  container: {
    paddingLeft: "20px",
    paddingRight: "20px"
  }
}

const styles = {
  background: {
    backgroundColor: "#EEE",
    margin: 0,
    padding: 0

  }, 
  content: {
    width: 600,
    marginLeft: "auto",
    marginRight: "auto",
    fontFamily: "Roboto"
  },
  topContainer: {
    paddingLeft: "auto",
    paddingRight: "auto",
    paddingTop: "20px",
    paddingBottom: "20px"
  },
  list: {
    card: {
      ...defaults.container,
      paddingTop: "20px",
      paddingBottom: "20px"
    },
    divider: {
      marginTop: "20px",
      marginBottom: "20px"
    }
  }
  
}

export default App;
