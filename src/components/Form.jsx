import React, {Fragment, Component} from 'react';
import {
  KeyboardArrowLeft,
  KeyboardArrowRight,
  DateRange,
  AccessTime

} from 'material-ui-icons';
import TextField from 'material-ui/TextField';
import {DateTimePicker} from 'material-ui-pickers';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';

const RequiredSymbol = () =>
  <span style={{marginRight: "20px"}}>*</span>

class Form extends Component {
  state = {
    valid: false,
    fields: {
      datetime: null,
      species: null,
      count: 0,
      description: "",
    }
  }

  formChanged(setField) {
    const {
      fields, 
      fields: {
        date, time, species, count, description
      }
    } = this.state
    this.setState({
      fields: {
        ...fields,
        ...setField
      }
    })
    const valid = 
         date 
      && time
      && species
      && (count > 0)
    
    if (valid) {
      this.props.formValidChange(valid)
    }

    this.setState({
      valid
    })
  }

  render() {
    const pickerIcons = {
      leftArrowIcon: <KeyboardArrowLeft/>,
      rightArrowIcon: KeyboardArrowRight,
      dateRangeIcon: DateRange,
      timeIcon: AccessTime
    }
    const {datetime} = this.state
    return (
      <Fragment>
        <DateTimePicker
          label="Set date and time"
          {...pickerIcons}
          value={datetime}
          onChange={(datetime) => this.formChanged({datetime: datetime})}
          disableFuture
        />
        <br/>
        <br/>
        <div>
          {
            JSON.stringify(this.state.fields, null, 2)
          }
        </div>
      </Fragment>
    )
  }
}

export default Form;
