import React, {Component} from 'react';

const Label = ({label}) => 
  <span>
    {label + ": "}
  </span>

const Description = ({description, style}) =>
  <div style={{
    width: "100%",
    ...style
  }}>
    {description}
  </div>

export default class Sighting extends Component {
  render() {
    const {sighting} = this.props
    return (
      <div>
        <Label label="Id" />{sighting.id}<br/>
        <Label label="Species" />{sighting.species}<br/>
        <Label label="Count" />{sighting.count}<br/>
        <Label label="Description" /><Description description={sighting.description} />
      </div>
    )
  }
}